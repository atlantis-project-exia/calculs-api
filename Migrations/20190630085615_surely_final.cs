﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace calculs_api.Migrations
{
    public partial class surely_final : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "devices_id_device",
                table: "ComputedItems",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "devices_id_device",
                table: "ComputedItems");
        }
    }
}
