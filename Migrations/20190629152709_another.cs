﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace calculs_api.Migrations
{
    public partial class another : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "value_computed",
                table: "ComputedItems",
                nullable: false,
                oldClrType: typeof(byte[]),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "value_computed",
                table: "ComputedItems",
                nullable: true,
                oldClrType: typeof(float));
        }
    }
}
