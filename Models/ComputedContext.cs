using Microsoft.EntityFrameworkCore;

namespace calculs_api.Models
{
    public class ComputedContext: DbContext
    {
        public ComputedContext(DbContextOptions<ComputedContext> options) : base(options)
        {

        }

        public DbSet<ComputedItem> ComputedItems {get;set;}
    }
}