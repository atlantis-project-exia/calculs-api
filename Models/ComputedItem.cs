using System;

namespace calculs_api.Models
{
    public class ComputedItem
    {
        public Guid Id {get;set;}
        public Guid computed_id_computed {get;set;}
        public float value_computed {get;set;}
        public string type_computed {get;set;}
        public DateTime created_at_computed {get;set;}
        public Guid devices_id_device {get;set;}
    }
}