using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using calculs_api.Models;

namespace calculs_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComputedController : ControllerBase
    {
        private readonly ComputedContext _context;

        public ComputedController(ComputedContext context)
        {
            _context = context;

            if (_context.ComputedItems.Count() == 0) {
                // Create a new ComputedItem if collection is empty,
                // which means you can't delete all ComputedItems.
                _context.ComputedItems.Add(new ComputedItem { Id = new Guid("00000000-0000-0000-0000-000000000000"), computed_id_computed = new Guid("00000000-0000-0000-0000-000000000000"), value_computed = -1, type_computed = "First Entry", created_at_computed = DateTime.Now});
                _context.SaveChanges();
            }
        }

        //GET: calculs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ComputedItem>>> GetComputedItems()
        {
            return await _context.ComputedItems.ToListAsync();
        }

        // Get: calculs/2019-06-27 15:05:00.568318/2019-06-29 15:05:00.568318/e46b2c10-20a2-4a13-9dec-db7e7ba8dcec
        [HttpGet("{start:DateTime}/{end:DateTime}/{dId:Guid}")]
        public async Task<ActionResult<List<ComputedItem>>> GetComputedItemsByDevice(DateTime start, DateTime end, Guid dId)
        {
            var computedItems = await _context.ComputedItems.Where(x => x.devices_id_device == dId && x.created_at_computed.CompareTo(start) >= 0 && x.created_at_computed.CompareTo(end) <= 0).ToListAsync();

            if (computedItems == null)
            {
                return NotFound();
            }

            return computedItems;
        }
    }
}